# umigap


*umigap* is an art-as-code tool for managing 3D art assets in indie games. It uses yaml files to describe the pipeline for importing a 3D character with mocap into a game so that the process is repeatable, tweakable and storable.

It does not do the autorigging or the mocap recording. This project is solely for developers to track
art assets as they move through the pipeline, so they can recreate the transformations of the objects
in a repeatable way.

Where a tool is available, it will make use of it. For example, if blender is installed as a python
module on the system, *umigap* can handle decimation, rotation, transformation and moving pivot points.

# Table of Contents
* [Quickstart](#quick-start)
* [Detailed Usage](#detailed-usage)
* [How do I...?](#how-do-i)
* [Requirements](#requirements)
* [Command line](#command-line)
* [About](#about)
* [Contributors](#contributors)
* [Development](#development)

# Quick Start

1. You have to set up the environment to run python scripts in blender (eg 'pip install bpy && bpy_post_install'). That is outside the scope of this project.
2. Install umigap 

`pip install umigap`

3. Initialise a umigap project for tracking your art asset pipeline

`umigap init`

This will create a directory and a YAML file.

4. Add a character you wish to track through the art asset pipeline

`umigap character add --project my-project`

Note: Does not copy the character asset, just links to it.

4a. Set some transform options on the character

`umigap character touch --character character --project my-project`

Will add an empty transform/scale/rotate object to the character in the yaml file.


5. Import the character

`umigap character import path-to-character-asset --character my-character --project my-project`

Will transform and create at least a .blend file in <project>/ready_to_rig 

6. (Auto)rig the character

`umigap character rig --character my-character --project my-project`

7. Add some mocap data into the pipeline

`umigap mocap add --mocap my-mocap --project my-project`

8. Import the mocap data

`umigap mocap import path-to-mocap-data --mocap my-mocap --project my-project`

9. Connect a mocap object with a character

`umigap character connect --project my-project`

10. Apply the mocap to the character to create an animated character

`umigap character animate --character my-character --project my-project`

11. Publish the character to your game

`umigap publish --character my-character --project my-project`

12. All done. Once you have done this process once, your pipeline is now set up and you can repeat it.

For example, to import all your art again, transform it, autorig it, apply mocap and publish into your game:

`umigap publish all --project my-project`



# Detailed Usage
Edit the yaml file to describe your pipeline and check that yaml into your repo.

# How do I... ?

## Add a new art asset to pipeline?

From the command-line:
`umigap character add --project my-project`


## Rotate a character 90 degrees?

In the yaml file add the `rotate` vector to the character's raw entry:


```
characters:
  leo:
    name: leo
    raw:
      rotate:
        x: 0
        y: 180
        z: 0
```

## Create bvh from raw mocap data?

That is not part of umigap, which is more of a metadata tool.

Record your mocap in something like perception neuron and export BVH data manually. However,
record the details of how you used the tool in umigap (eg the start frame and end from of the export)
so you can recreate it if needed.



# Requirements

Unfortunately there are a lot of moving parts to a 3D pipeline so the versioning can get very specific.

* python 3.7+
* bpy 2.82.x 
* blender 2.93.x 
* auto_rig_pro_3.60 (not 3.63+)


# Command line

## Characters
```
Usage: umigap character [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  add      Add a character to the pipeline for a project
  connect  Connect a mocap dataset to a character in a project
  import   Import the raw 3D file and transform it.
```

### Examples
`umigap character add --character leo --project game2022`

`umigap character import ~/projects/artist/leo/raw_mesh.glb --character leo --project game2022`


# About

*umigap* stands for Up Multimedia Indie Game Asset Pipeline. It is used by our small studio to 
programmatically transform raw 3D meshes into finished animated game-ready characters for our game,
The Beat. The aesthetic of that game is organic procedural, so the flaws and roughness of an
automated pipeline is of great attraction to us.

The Beat made with the support of Screen Vic. 

# Contributors

* Luke Miller

# Development

*umigap* uses `pyproject.toml` (the modern replacement for setup.py) for configure and `poetry` to handle the config.

Our coding house style is to apply isort, black and flake8 (in that order) in the pre-commit hook with an 80% 
code coverage gate. See the environment/ directory for the git pre-commit hook sample.

## Test

`poetry run pytest umgapi/*_test.py`

