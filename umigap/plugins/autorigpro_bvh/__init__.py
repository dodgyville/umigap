import os
import subprocess
from pathlib import Path


def apply_bvh(
    input_file, output_path, animation_details_str, mocap_details_str, formats
):
    fpath = Path(os.path.dirname(__file__)) / "apply.py"
    subprocess.run(
        [
            "blender",
            "-b",
            "-P",
            fpath,
            "--",
            input_file,
            output_path,
            animation_details_str,
            mocap_details_str,
            *formats,
        ]
    )


def run(obj, config, data: dict):
    print("running arp bvh plugin", data)
    character_slug = data["character_slug"]
    animation_slug = data["animation_slug"]
    # character = obj.characters[character_slug]
    mocap_detail = obj.mocaps[data["mocap_slug"]]
    animation_detail = obj.animations[data["animation_slug"]]
    rig_detail = obj.rigs[character_slug]
    input_file = Path(rig_detail.output)
    if not input_file.is_absolute():  # make it relative to the cwd
        input_file = Path(data.get("cwd"), ".") / Path(rig_detail.output)
    final_path = Path(data.get("cwd"), ".") / Path(obj.slug) / Path("final")
    output_path = (
        final_path / Path(character_slug) / Path(animation_slug).with_suffix(".glb")
    )
    formats = [".glb"]
    apply_bvh(
        input_file,
        output_path,
        animation_detail.to_json(),
        mocap_detail.to_json(),
        formats,
    )
