import json
import sys
import warnings
from pathlib import Path

from bvh import Bvh

from umigap.plugins.blender_transform.core import (
    bpy,
    load_3d,
    redefine_rest_pose,
    save_3d,
    select_source_armature,
    select_target_armature,
    set_root_bone_mapping,
)


def apply_bvh(input_path, output_path, animation_details, mocap_details, formats):
    """apply bvh"""
    print(" apply bvh to ", input_path)
    loaded = load_3d(input_path)
    if not loaded:
        return False

    bvh_filename = Path(mocap_details["output"])

    # get some details of the bvh
    with open(bvh_filename) as f:
        mocap = Bvh(f.read())
        bvh_frame_time = mocap.frame_time
        bvh_fps = 1 / bvh_frame_time

    bpy.ops.import_anim.bvh(
        filepath=bvh_filename.as_posix(), use_fps_scale=True
    )  # , global_scale=0.01)

    # animation length (bvh length and also final blender animation length)
    start = animation_details.get("start", None)
    end = animation_details.get("end", None)
    if not start:
        start = 0
    if not end:
        end = mocap.nframes

    blender_fps = bpy.context.scene.render.fps

    print("start frame", start, " end frame", end, " bvh_fps", bvh_fps)
    bvh_duration = end / bvh_fps

    new_frame_end = bvh_duration * blender_fps
    print(
        " old blender frame end",
        bpy.data.scenes["Scene"].frame_end,
        " new frame_end:",
        new_frame_end,
    )
    # bpy.data.scenes["Scene"].frame_end = new_frame_end
    scn = bpy.context.scene
    scn.frame_start = 0
    scn.frame_end = new_frame_end

    # select source armature (bvh)
    select_source_armature(bvh_filename.stem)

    # select target armature (autorig)
    select_target_armature("rig")

    # auto-scale
    print("auto-scale")
    bpy.ops.arp.auto_scale()

    print("building bones list...")
    bpy.ops.arp.build_bones_list()

    print("set the root bone mapping...")
    set_root_bone_mapping()

    print("redefine armatures rest pose...")
    redefine_rest_pose(bvh_filename.stem)

    # retarget!
    print(f"retarget mocap data to character... ({0}:{new_frame_end})")
    bpy.ops.arp.retarget(frame_start=start, frame_end=new_frame_end)

    #    export_name = Path("final/countrykid_flourish_autotest-2021-11-6-1836.blend")
    #    export_name = Path("final/countrykid_flourish_test.glb")
    # export_name = get_final_path(character, anim)
    export_name = Path(output_path)
    if export_name.exists():
        warnings.warning(f"{export_name} already exists.")
    # bpy.ops.export_scene.gltf(filepath=export_name)
    save_3d(export_name)
    save_3d(export_name.with_suffix(".blend"))

    print("done remap")
    return True


if __name__ == "__main__":
    print(f"Start bvh apply using {sys.argv}")
    (
        input_path,
        output_path,
        animation_details_str,
        mocap_details_str,
        *formats,
    ) = sys.argv[5:]
    animation_details = json.loads(animation_details_str)
    mocap_details = json.loads(mocap_details_str)
    apply_bvh(input_path, output_path, animation_details, mocap_details, formats)
    print(f"Finish bvh to formats {formats}")
