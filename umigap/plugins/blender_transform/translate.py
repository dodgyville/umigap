import sys
from pathlib import Path

from umigap.plugins.blender_transform.core import bpy, load_3d, save_3d


def translate_scene(x=0, y=0, z=0):
    """translate meshes"""
    object_list = bpy.data.objects
    print(f" will translate {x}, {y}, {z}")

    for obj in object_list:
        if obj.type == "MESH":
            obj.location[0] += x
            obj.location[1] += y
            obj.location[2] += z


def process_character(input_file, output_file, x, y, z, formats):
    """Load the file and process"""
    loaded = load_3d(input_file)
    if not loaded:
        return False

    translate_scene(x, y, z)

    output_file = Path(output_file)

    for file_format in formats:
        save_path = output_file.with_suffix(file_format)
        print(" Save to", save_path)
        save_3d(save_path)


if __name__ == "__main__":
    print("Start translate", sys.argv)
    input_path, output_path, x, y, z, *formats = sys.argv[5:]
    process_character(input_path, output_path, float(x), float(y), float(z), formats)
    print("Finish translate to formats", formats)
