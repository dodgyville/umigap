import json
import sys

from umigap.plugins.blender_transform.core import bpy, process_character


def scale_scene(transform_details):
    """scale meshes"""
    print(" scale mesh ", transform_details)
    scale_factor = float(transform_details)

    object_list = bpy.data.objects

    for obj in object_list:
        if obj.type == "MESH":
            obj.scale *= scale_factor
            # bpy.ops.transform


if __name__ == "__main__":
    transformation = "scale"
    transform_fn = scale_scene
    print(f"Start {transformation} using {sys.argv}")
    input_path, output_path, decimate_details_str, *formats = sys.argv[5:]
    new_details = json.loads(decimate_details_str)
    process_character(input_path, output_path, new_details, formats, transform_fn)
    print(f"Finish {transformation} to formats {formats}")
