"""
Take some data from umigap describing a 3D mesh and some modifications, and use blender to apply those modifications

    # drop pivots
    if details.get("drop-pivot", False):
        print("dropping pivot")
        drop_pivot_details = []
        drop_pivot(drop_pivot_details)

    # volume pivots
    if details.get("volume-pivot", False):
        print("ignoring volume-pivot request")

    # scale
    scale_factor = details.get("scale", 1.0)
    if scale_factor != 1.0:
        print("scaling")
        scale_scene(scale_factor)

    # hide layers
    layers = details.get("disable-layers", [])
    if layers:
        print("disabling layers")
        hide_layers(layers)


"""
import json
import shutil
import subprocess
import warnings
from pathlib import Path


def run_decimate(input_file, output_path, details_str, formats):
    subprocess.run(
        [
            "blender",
            "-b",
            "-P",
            "umigap/plugins/blender_transform/decimate.py",
            "--",
            input_file,
            output_path,
            details_str,
            *formats,
        ]
    )


def run_rotate(input_file, output_path, details_str, formats):
    subprocess.run(
        [
            "blender",
            "-b",
            "-P",
            "umigap/plugins/blender_transform/rotate.py",
            "--",
            input_file,
            output_path,
            details_str,
            *formats,
        ]
    )


def run_scale(input_file, output_path, details_str, formats):
    subprocess.run(
        [
            "blender",
            "-b",
            "-P",
            "umigap/plugins/blender_transform/scale.py",
            "--",
            input_file,
            output_path,
            details_str,
            *formats,
        ]
    )


def run_translate(input_file, output_path, x, y, z, formats):
    subprocess.run(
        [
            "blender",
            "-b",
            "-P",
            "umigap/plugins/blender_transform/translate.py",
            "--",
            input_file,
            output_path,
            x,
            y,
            z,
            *formats,
        ]
    )


def run(obj, config, data: dict):
    print("running blender transform plugin", data)
    character_slug = data["character_slug"]
    character = obj.characters[character_slug]
    raw_detail = obj.raw[data["raw_slug"]]
    input_file = Path(character.input)
    if not input_file.is_absolute():  # make it relative to the cwd
        input_file = Path(data.get("cwd"), ".") / Path(character.input)
    ready_to_rig_path = (
        Path(data.get("cwd"), ".") / Path(obj.slug) / Path("ready_to_rig")
    )
    output_path = (
        ready_to_rig_path
        / Path(character_slug)
        / Path(character_slug).with_suffix(".blend")
    )
    formats = config.formats
    working_formats = [".blend"]

    if raw_detail.decimate_layers:
        transform_str = json.dumps(raw_detail.decimate_layers)
        run_decimate(input_file, output_path, transform_str, working_formats)
    else:
        shutil.copy(input_file, output_path)

    # do all subsequent transforms on the output file
    input_file = output_path

    if raw_detail.rotate:
        transform_str = raw_detail.rotate.to_json()
        run_rotate(input_file, output_path, transform_str, working_formats)

    if raw_detail.translate:
        x, y, z = raw_detail.translate
        run_translate(input_file, output_path, str(x), str(y), str(z), working_formats)

    if raw_detail.volume_pivot_layers:
        warnings.warn("Ignoring volume_pivot_layers")

    # last step, so output all the requested file formats
    if raw_detail.scale:
        transform_str = str(raw_detail.scale)
        run_scale(input_file, output_path, transform_str, formats)
    else:
        transform_str = "1.0"
        run_scale(input_file, output_path, transform_str, formats)

    print("finished blender transform plugin")
