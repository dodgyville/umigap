import json
import sys
from math import radians

from umigap.plugins.blender_transform.core import bpy, process_character


def rotate_scene(transform_details):
    """rotate meshes"""
    print(" rotate mesh ", transform_details)
    x, y, z = transform_details["x"], transform_details["y"], transform_details["z"]
    object_list = bpy.data.objects
    ov = bpy.context.copy()
    ov["area"] = [a for a in bpy.context.screen.areas if a.type == "VIEW_3D"][0]
    bpy.ops.transform.rotate(ov)
    for obj in object_list:
        if obj.type == "MESH":
            if x:
                bpy.ops.transform.rotate(ov, value=radians(x), orient_axis="X")
            if y:
                bpy.ops.transform.rotate(ov, value=radians(y), orient_axis="Y")
            if z:
                bpy.ops.transform.rotate(ov, value=radians(z), orient_axis="Z")


if __name__ == "__main__":
    transformation = "rotate"
    transform_fn = rotate_scene
    print(f"Start {transformation} using {sys.argv}")
    input_path, output_path, decimate_details_str, *formats = sys.argv[5:]
    new_details = json.loads(decimate_details_str)
    process_character(input_path, output_path, new_details, formats, transform_fn)
    print(f"Finish {transformation} to formats {formats}")
