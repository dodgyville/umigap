import json
import sys
from pathlib import Path

from umigap.plugins.blender_transform.core import (
    bpy,
    load_3d,
    process_character,
    save_3d,
)


def decimate_scene(decimate_details):
    """
    Reduce a mesh collection to the amount in decimate (0-1.0)
    """
    object_list = bpy.data.objects
    print("\n preparing for decimate...", decimate_details)
    original_num_vertices = 0
    new_num_vertices = 0
    for obj in object_list:
        if obj.type == "MESH":
            me = obj.data
            vs = len(me.vertices)
            original_num_vertices += vs
            es = len(me.edges)
            ps = len(me.polygons)  # faces
            ratio = decimate_details.get(obj.name, decimate_details.get("default"))
            print(
                f" decimate {obj.name} verts: {vs}, edges:{es}, polygons: {ps}, ratio: {ratio}"
            )

    print("\n beginning decimate...")
    for obj in object_list:
        if obj.type == "MESH":
            me = obj.data
            vs = len(me.vertices)
            es = len(me.edges)
            ps = len(me.polygons)  # faces
            print(f" decimate {obj.name} verts: {vs}, edges:{es}, polygons: {ps}")
            # bpy.ops.object.modifier_add(type='DECIMATE')

            modifier_name = "DecimateMod"
            modifier = obj.modifiers.new(modifier_name, "DECIMATE")

            modifier.ratio = decimate_details.get(
                obj.name, decimate_details.get("default")
            )
            print(f"  add {modifier.ratio} decimate ratio to {obj.name}")

            modifier.use_collapse_triangulate = True
            #            bpy.context.scene.objects.active = obj  # blender 2.7x
            bpy.context.view_layer.objects.active = obj  # blender 2.8x
            bpy.ops.object.modifier_apply(modifier=modifier_name)

            vs = len(me.vertices)
            es = len(me.edges)
            ps = len(me.polygons)  # faces
            new_num_vertices += vs
            print(
                f"  after decimate {obj.name} verts: {vs}, edges:{es}, polygons: {ps}"
            )

            # bpy.context.object.modifiers["Decimate"].use_collapse_triangulate=True
            # bpy.context.object.modifiers["Decimate"].ratio = decimate
    print(
        f" Finished decimate. Reduced from {original_num_vertices} vertices to {new_num_vertices}."
    )


if __name__ == "__main__":
    print("Start decimate", sys.argv)
    input_path, output_path, decimate_details_str, *formats = sys.argv[5:]
    decimate_details = json.loads(decimate_details_str)
    process_character(
        input_path, output_path, decimate_details, formats, decimate_scene
    )
    print("Finish decimate to formats", formats)
