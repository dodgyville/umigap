"""
Core utilities for blender
"""

import logging
import sys
import warnings
from pathlib import Path

try:
    import bpy  # blender
except ModuleNotFoundError:
    bpy = None
    print("Unable to find blender python module")
    print(
        "Run from a script with something like: blender -b -P godotrig.py -- infile character -b bvh -d"
    )
    sys.exit("Blender not available")


logger = logging.getLogger(__name__)


def get_bone_index(name):
    """get a bone map index in blender"""
    for i, bone in enumerate(bpy.data.scenes["Scene"].bones_map):
        if bone.source_bone == name:
            print("found bone", name, "at", i)
            return i
    return None


def select_bones_by_names(armature, names):
    """select bones by names in blender"""
    print("select bones by names")
    found = []
    armature = [
        x for x in bpy.data.objects if x.type == "ARMATURE" and x.name == armature
    ][0]
    pose_bones = armature.pose.bones
    for pose_bone in pose_bones:
        if pose_bone.name in names:
            pose_bone.bone.select = True
            # pose_bone.bone.active = True
            found.append(pose_bone.name)
            print("found bone", pose_bone.name)
    if len(found) < len(names):
        print("unable to find all names", [item for item in names if item not in found])


def set_root_bone_mapping():
    """
    set the root bone mapping, eg which bone controls the motion of the skeleton (needed for moving across floors)
    """
    bone_index = get_bone_index("Hips")
    # bone = bpy.data.scenes["Scene"].bones_map[bone_index]
    select_bones_by_names("rig", ["c_root_master.x"])
    bpy.ops.arp.pick_object(action="pick_bone")

    scn = bpy.context.scene
    # bones_map = scn.bones_map
    scn.bones_map_index = bone_index
    bpy.data.scenes["Scene"].bones_map[bone_index].set_as_root = True


def deselect_all():
    """deselect everything in blender"""
    bpy.ops.object.select_all(action="DESELECT")


def select_armature(name):
    """select armature by name in blender"""
    for obj in bpy.context.scene.objects:
        found = (obj.type == "ARMATURE") and (obj.name == name)
        if found:
            print("selected", name)
            obj.select_set(True)
            return
    print("Unable to find", name, "to select")


def select_source_armature(armature_name):
    """Select an armature (eg a track of bvh data)"""
    deselect_all()
    select_armature(armature_name)
    bpy.ops.arp.pick_object(action="pick_source")


def select_target_armature(name):
    """Select the character armature we want to apply bvh data to"""
    deselect_all()
    select_armature(name)
    # bpy.ops.arp.pick_object(action="pick_target")  # Doesn't appear to work, trying again Nov 2021
    bpy.data.scenes["Scene"].target_rig = name


def redefine_rest_pose(source_armature_name):
    """make the target mesh and the bvh skeleton bones match up better"""
    bpy.ops.arp.redefine_rest_pose()
    # select Head, Neck, LeftArm, LeftForeArm, RightArm, RightForeArm, RightUpLeg, LeftUpLeg, RightLeg, LeftLeg
    # deselect_all()
    select_bones_by_names(
        source_armature_name,
        [
            "Head",
            "Neck",
            "LeftArm",
            "LeftForeArm",
            "RightArm",
            "RightForeArm",
            "RightUpLeg",
            "LeftUpLeg",
            "RightLeg",
            "LeftLeg",
            "LeftFoot",
            "RightFoot",
        ],
    )

    # Maybe need to do feet?
    bpy.ops.arp.copy_bone_rest()  # copy selected bones
    bpy.ops.arp.copy_raw_coordinates()  # apply


def load_3d(filename):
    """
    Load a 3D file into blender. Clears existing scene.
    """
    filename = Path(filename)
    print("*****start load", filename)
    if not filename.exists():
        print("not found")
        warnings.warn(f"Unable to load {filename}. File not found")
        logger.warning(f"Unable to load {filename}. File not found")
        return False
    print(f"loading {filename}")
    bpy.ops.wm.read_homefile(use_empty=True)  # clear scene
    if "fbx" in filename.suffix:
        bpy.ops.import_scene.fbx(filepath=filename.as_posix())
    elif "dae" in filename.suffix:
        bpy.ops.wm.collada_import(filepath=filename.as_posix())
    elif "glb" in filename.suffix:
        bpy.ops.import_scene.gltf(filepath=filename.as_posix())
    elif "gltf" in filename.suffix:
        bpy.ops.import_scene.gltf(filepath=filename.as_posix())
    elif "blend" in filename.suffix:
        bpy.ops.wm.open_mainfile(filepath=filename.as_posix())
    else:
        logger.warning("File format not recognised. FIle not loaded")
        return False
    return True


def save_3d(filename):
    """
    save a 3D file
    """
    filename = Path(filename)
    if filename.exists():
        logger.warning(f"{filename} already exists")
    print(f"saved {filename}")
    if "escn" in filename.suffix:
        bpy.ops.export_godot.escn(filepath=filename.as_posix())
    elif "glb" in filename.suffix:
        bpy.ops.export_scene.gltf(filepath=filename.as_posix())
    elif "gltf" in filename.suffix:
        bpy.ops.export_scene.gltf(
            filepath=filename.as_posix(), export_format="GLTF_EMBEDDED"
        )
    elif "fbx" in filename.suffix:
        bpy.ops.export_scene.fbx(filepath=filename.as_posix())
    elif "dae" in filename.suffix:
        bpy.ops.wm.collada_export(filepath=filename.as_posix())
    elif "blend" in filename.suffix:
        bpy.ops.wm.save_as_mainfile(filepath=filename.as_posix())
    else:
        logger.warning("File format not recognised. FIle not saved")
        return False
    return True


def process_character(input_file, output_file, details, file_formats, process_fn):
    """Load the file and process"""
    loaded = load_3d(input_file)
    if not loaded:
        return False

    process_fn(details)

    output_file = Path(output_file)

    for file_format in file_formats:
        save_path = output_file.with_suffix(file_format)
        print(" Save to", save_path)
        save_3d(save_path)
