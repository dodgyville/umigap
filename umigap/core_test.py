from .core import UMIGAP, Tool, available_plugins


class TestCore:
    def test_find_tool_for_stage(self):
        obj = UMIGAP("test")
        obj.add_tool("my_tool", Tool(stage="rig"))
        assert obj.find_tool_for_stage("rig") is not None


def test_available_plugins():
    assert len(available_plugins()) == 6
